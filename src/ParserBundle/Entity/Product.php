<?php

namespace ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Product
 *
 * @ORM\Table(name="Product")
 * @ORM\Entity(repositoryClass="ParserBundle\Entity\ProductRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="code", type="bigint", unique=true)
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="ProductVariant", inversedBy="Product")
     */
    private $variant;

    /**
     * @var string
     *
     * @ORM\Column(name="internal_description", type="string", length=255, nullable=true)
     */
    private $internalDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="app_description", type="string", length=255, nullable=true)
     */
    private $appDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="SFDC_description", type="string", length=255, nullable=true)
     */
    private $SFDCDescription;

    /**
     * @var float
     *
     * @ORM\Column(name="RRP", type="decimal", precision=6, scale=3)
     */
    private $RRP;

    /**
     * @var float
     *
     * @ORM\Column(name="RRP_with_GST7_percent", type="decimal", precision=6, scale=3)
     */
    private $RRPWithGST7Percent;

    /**
     * @ORM\ManyToOne(targetEntity="ProductType", inversedBy="Product")
     */
    private $type;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted", type="boolean", options={"default" : 0})
     */
    private $deleted = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Brand", inversedBy="Product")
     */
    private $brand;

    /**
     * @ORM\ManyToOne(targetEntity="Package", inversedBy="Product")
     */
    private $package;

    /**
     * @ORM\ManyToOne(targetEntity="ProductSize", inversedBy="Product")
     */
    private $size;

    /**
     * @ORM\Column(name="unit", type="integer")
     */
    private $unit;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param integer $code
     * @return Product
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return integer 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set variant
     *
     * @param ProductVariant $variant
     * @return Product
     */
    public function setVariant(ProductVariant $variant)
    {
        $this->variant = $variant;

        return $this;
    }

    /**
     * Get variant
     *
     * @return ProductVariant
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * Set internalDescription
     *
     * @param string $internalDescription
     * @return Product
     */
    public function setInternalDescription($internalDescription)
    {
        $this->internalDescription = $internalDescription;

        return $this;
    }

    /**
     * Get internalDescription
     *
     * @return string 
     */
    public function getInternalDescription()
    {
        return $this->internalDescription;
    }

    /**
     * Set appDescription
     *
     * @param string $appDescription
     * @return Product
     */
    public function setAppDescription($appDescription)
    {
        $this->appDescription = $appDescription;

        return $this;
    }

    /**
     * Get appDescription
     *
     * @return string 
     */
    public function getAppDescription()
    {
        return $this->appDescription;
    }

    /**
     * Set SFDCDescription
     *
     * @param string $SFDCDescription
     * @return Product
     */
    public function setSFDCDescription($SFDCDescription)
    {
        $this->SFDCDescription = $SFDCDescription;

        return $this;
    }

    /**
     * Get SFDCDescription
     *
     * @return string 
     */
    public function getSFDCDescription()
    {
        return $this->SFDCDescription;
    }

    /**
     * Set rRP
     *
     * @param float $rRP
     * @return Product
     */
    public function setRRP($RRP)
    {
        $this->RRP = $RRP;

        return $this;
    }

    /**
     * Get RRP
     *
     * @return float 
     */
    public function getRRP()
    {
        return $this->RRP;
    }

    /**
     * Set RRPWithGST7Percent
     *
     * @param float $RRPWithGST7Percent
     * @return Product
     */
    public function setRRPWithGST7Percent($RRPWithGST7Percent)
    {
        $this->RRPWithGST7Percent = $RRPWithGST7Percent;

        return $this;
    }

    /**
     * Get RRPWithGST7Percent
     *
     * @return float 
     */
    public function getRRPWithGST7Percent()
    {
        return $this->RRPWithGST7Percent;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param ProductType $type
     *
     * @return Product
     */
    public function setType(ProductType $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime('now');

    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime('now');

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param mixed $deleted
     *
     * @return Product
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }
    /**
     * @return Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     *
     * @return Product
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return Package
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param Package $package
     *
     * @return Product
     */
    public function setPackage(Package $package)
    {
        $this->package = $package;

        return $this;
    }

    /**
     * @return ProductSize
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param ProductSize $size
     *
     * @return Product
     */
    public function setSize(ProductSize $size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return integer
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param mixed $unit
     *
     * @return Product
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

}
