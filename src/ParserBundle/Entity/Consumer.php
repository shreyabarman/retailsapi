<?php

namespace ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;

/**
 * Consumer
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ParserBundle\Entity\ConsumerRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Consumer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ucc", type="bigint", unique=true)
     */
    private $ucc;

    /**
     * @var string
     *
     * @ORM\Column(name="name_1", type="string", length=255)
     */
    private $name1;

    /**
     * @var string
     *
     * @ORM\Column(name="name_2", type="string", length=255, nullable=true)
     */
    private $name2;

    /**
     * @var string
     *
     * @ORM\Column(name="distributor", type="string", length=255)
     */
    private $distributor;


    /**
     * @var string
     *
     * @ORM\Column(name="address_1", type="string", length=255)
     */
    private $address1;

    /**
     * @var string
     *
     * @ORM\Column(name="address_2", type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted", type="boolean", options={"default" : 0})
     */
    private $deleted = 0;


    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="consumer")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="PostalDetail", inversedBy="Consumer")
     */
    private $postalDetail;

    /**
     * @ORM\ManyToOne(targetEntity="City", inversedBy="Consumer")
     */
    private $city;

    /**
     * @return PostalDetail
     */
    public function getPostalDetail()
    {
        return $this->postalDetail;
    }

    /**
     * @param PostalDetail $postalDetail
     *
     * @return Consumer
     */
    public function setPostalDetail(PostalDetail $postalDetail)
    {
        $this->postalDetail = $postalDetail;

        return $this;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city
     *
     * @return Consumer
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getName1()
    {
        return $this->name1;
    }

    /**
     * @param string $name1
     *
     * @return Consumer
     */
    public function setName1($name1)
    {
        $this->name1 = $name1;

        return $this;
    }

    /**
     * @return string
     */
    public function getName2()
    {
        return $this->name2;
    }

    /**
     * @param string $name2
     *
     * @return Consumer
     */
    public function setName2($name2)
    {
        $this->name2 = $name2;

        return $this;
    }

    /**
     * @return string
     */
    public function getDistributor()
    {
        return $this->distributor;
    }

    /**
     * @param string $distributor
     *
     * @return Consumer
     */
    public function setDistributor($distributor)
    {
        $this->distributor = $distributor;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param string $address1
     *
     * @return Consumer
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param string $address2
     *
     * @return Consumer
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime('now');

    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime('now');

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param mixed $deleted
     *
     * @return Consumer
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return Consumer
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ucc
     *
     * @param integer $ucc
     * @return Consumer
     */
    public function setUcc($ucc)
    {
        $this->ucc = $ucc;

        return $this;
    }

    /**
     * Get ucc
     *
     * @return integer
     */
    public function getUcc()
    {
        return $this->ucc;
    }
}
