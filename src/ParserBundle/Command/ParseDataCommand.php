<?php
/**
 * configures a command for symfony console to parse product and consumer data
 *
 * @author shreya
 * @category Command
 * @since August, 2018
 */
namespace ParserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ParseDataCommand
 * @package ParserBundle\Command
 */
class ParseDataCommand extends ContainerAwareCommand
{
    /**
     * sets name and description of command
     */
    protected function configure()
    {
        $this->setName('parser:parseData')
            ->setDescription('insert data from consumer and product csv file');
    }

    /**
     * initiates services to insert data into database from csv
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dataInsertService =  $this->getContainer()->get('parser.CSV_parser');

        $output->writeln('Loading Products......');
        $message = $dataInsertService->parseFile('Product.csv' , 'Product');
        $output->writeln($message);

        $output->writeln('Loading Consumers......');
        $message = $dataInsertService->parseFile('Consumer.csv' , 'Consumer');
        $output->writeln($message);

    }
}
