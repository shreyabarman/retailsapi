<?php
namespace ParserBundle\Service;

class ValidatorService
{
    static $consumerHeader = array("UCC" => -1, "CUSTOMER NAME 1" => -1,  "CUSTOMER NAME 2" => -1,  "ADDRESS 1" => -1,
        "ADDRESS 2" => -1,  "Post Code" => -1,  "City" => -1, "Distributor" => -1);

    static $productHeader = array("Type" => -1, "Product Code" => -1, "Brand" => -1, "Variant" => -1, "Size" => -1,
        "Packing" => -1, "Product Description(Internal)" => -1, "Product Description(App)" => -1,
        "Product Description(SFDC)" => -1, "Units per case" => -1, "RRP / Case without GST" => -1,
        "RRP / Case with GST 7%" => -1);

    public function validateConsumer($data)
    {
        $result['success'] = true;
        foreach ($data as $index => $value)
        {
            if(isset(ValidatorService::$consumerHeader[$value]))
            {
                ValidatorService::$consumerHeader[$value] = $index;
            }
        }
        if (in_array(-1, ValidatorService::$consumerHeader)) {
            $result['success'] = false;
            $result['code'] = 204;
            $result['error'] ='invalid csv';
        } else {
            $result['code'] = 200;
            $result['array'] = ValidatorService::$consumerHeader;
        }

        return json_encode($result);
    }

    public function validateProduct($data)
    {
        $result['success'] = true;
        foreach ($data as $index => $value)
        {
            if(isset(ValidatorService::$productHeader[$value]))
            {
                ValidatorService::$productHeader[$value] = $index;
            }
        }
        if (in_array(-1, ValidatorService::$productHeader)) {
            $result['success'] = false;
            $result['code'] = 204;
            $result['error'] ='invalid csv';
        } else {
            $result['code'] = 200;
            $result['array'] = ValidatorService::$productHeader;
        }

        return json_encode($result);
    }
}
