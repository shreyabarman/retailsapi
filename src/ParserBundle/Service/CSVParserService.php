<?php
/**
 * Parse through CSV files
 *
 * @author Shreya
 * @since August,2018
 * @category Service
 */
namespace ParserBundle\Service;

use Doctrine\ORM\EntityManager;
use ParserBundle\Service\ValidatorService;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Translation\Translator;
use ParserBundle\Entity\Product;
use ParserBundle\Entity\ProductRepository;

/**
 * Class CSVParserService
 * @package ParserBundle\Service
 */
class CSVParserService
{
    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var ValidatorService
     */
    protected $validatorService;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var InsertService
     */
    protected $insertService;

    /**
     * @var $errorlogger
     */
    protected $errorLogger;

    /**
     * @var $debugLogger
     */
    protected $debugLogger;

    /**
     * CSVParserService constructor.
     * @param Container $container
     * @param EntityManager $entityManager
     * @param ValidatorService $validatorService
     * @param InsertService
     * @param $errorLogger
     * @param $debugLogger
     *
     * @throws
     */
    public function __construct(Container $container, EntityManager $entityManager, ValidatorService $validatorService, InsertService $insertService, $errorLogger, $debugLogger)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
        $this->validatorService = $validatorService;
        $this->translator = $container->get('Translator');
        $this->insertService = $insertService;
        $this->errorLogger = $errorLogger;
        $this->debugLogger = $debugLogger;
    }

    public function parseFile($fileName, $type)
    {
        $columnIndex = array();
        $result = array();
        $isHeaderRow = 1;
        $row = array();
        $file_path = $this->container->getParameter('file_path') . $fileName;
        $validatorFunction = 'validate' . $type;

        if (($handle = fopen($file_path, 'r')) !== false) {

            while (($data = fgetcsv($handle)) !== false) {

                if ($isHeaderRow === 1) {
                    $columnIndex = json_decode($this->validatorService->$validatorFunction($data), true);

                    if (!$columnIndex[$this->translator->trans('success')]) {
                        $result[$this->translator->trans('success')] = false;
                        $result[$this->translator->trans('code')] = 204;
                        $result[$this->translator->trans('error')] = $columnIndex[$this->translator->trans('error')];

                        return json_encode($result);
                    }
                } else {
                    $result[$this->translator->trans('success')] = true;
                    foreach ($columnIndex['array'] as $key => $value) {
                        $row[$key] = $data[$value];
                    }

                    try {
                        $functionName = 'create' . $type;
                        $this->insertService->$functionName($row);

                    } catch (\Exception $e) {

                        $this->entityManager->getConnection()->rollback();
                        $result[$this->translator->trans('success')] = false;
                        $result[$this->translator->trans('error')] = $e->getMessage();
                        $result[$this->translator->trans('code')] = '500';
                        $this->errorLogger->error($e->getMessage());
                        $this->debugLogger->debug($e->getMessage());

                        return json_encode($result);
                    }
                }

                $isHeaderRow++;
            }
            fclose($handle);
        } else {
            $result[$this->translator->trans('success')] = false;
            $result[$this->translator->trans('error')] = $this->translator->trans('fileNotFound');
            $result[$this->translator->trans('code')] = '204';

            return json_encode($result);
        }

        $result[$this->translator->trans('success')] = true;
        $result[$this->translator->trans('message')] = $this->translator->trans('insertSuccess');
        $result[$this->translator->trans('code')] = '200';

        return json_encode($result);
    }
}


?>

