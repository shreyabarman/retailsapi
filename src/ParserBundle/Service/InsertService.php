<?php
namespace ParserBundle\Service;

use Doctrine\ORM\EntityManager;
use ParserBundle\Entity\Brand;
use ParserBundle\Entity\City;
use ParserBundle\Entity\Consumer;
use ParserBundle\Entity\Package;
use ParserBundle\Entity\PostalDetail;
use ParserBundle\Entity\Product;
use ParserBundle\Entity\ProductSize;
use ParserBundle\Entity\ProductType;
use ParserBundle\Entity\ProductTypeRepository;
use ParserBundle\Entity\ProductVariant;

class InsertService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * InsertService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createProduct($data)
    {
        $this->entityManager->beginTransaction();

        $product = new Product();

        $type = $this->insertType($data['Type']);
        $variant = $this->insertVariant($data['Variant']);
        $brand = $this->insertBrand($data['Brand']);
        $package = $this->insertPackage($data['Packing']);
        $size = $this->insertSize($data['Size']);

        $product->setCode($data['Product Code']);
        $product->setUnit($data['Units per case']);
        $product->setAppDescription($data['Product Description(App)']);
        $product->setInternalDescription($data['Product Description(Internal)']);
        $product->setSFDCDescription($data['Product Description(SFDC)']);
        $product->setRRPWithGST7Percent($data['RRP / Case with GST 7%']);
        $product->setRRP($data['RRP / Case without GST']);
        $product->setSize($size);
        $product->setType($type);
        $product->setBrand($brand);
        $product->setPackage($package);
        $product->setVariant($variant);

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        $this->entityManager->commit();
    }

    public function insertType($name)
    {
        $type = $this->entityManager->getRepository('ParserBundle:ProductType')
            ->findOneBy(array('name' => $name));

        if ($type === null) {
            $type = new ProductType();
            $type->setName($name);

            $this->entityManager->persist($type);
        }

        return $type;
    }

    public function insertVariant($name)
    {
        $variant = $this->entityManager->getRepository('ParserBundle:ProductVariant')
            ->findOneBy(array('name' => $name));

        if ($variant === null) {
            $variant = new ProductVariant();
            $variant->setName($name);

            $this->entityManager->persist($variant);
        }

        return $variant;
    }

    public function insertPackage($name)
    {
        $package = $this->entityManager->getRepository('ParserBundle:Package')
            ->findOneBy(array('package' => $name));

        if ($package === null) {
            $package = new Package();
            $package->setPackage($name);

            $this->entityManager->persist($package);
        }

        return $package;
    }

    public function insertBrand($name)
    {
        $brand = $this->entityManager->getRepository('ParserBundle:Brand')
            ->findOneBy(array('name' => $name));

        if ($brand === null) {
            $brand = new Brand();
            $brand->setName($name);

            $this->entityManager->persist($brand);
        }

        return $brand;
    }

    public function insertSize($size)
    {
        $productSize = $this->entityManager->getRepository('ParserBundle:ProductSize')
            ->findOneBy(array('size' => $size));

        if ($productSize === null) {
            $productSize = new ProductSize();
            $productSize->setSize($size);

            $this->entityManager->persist($productSize);
        }

        return $productSize;
    }

    public function createConsumer($data)
    {
        $this->entityManager->beginTransaction();
        $consumer = new Consumer();

        $city = $this->insertCity($data['City']);
        $postalDetail = $this->insertPostalDetail($data['Post Code']);

        $consumer->setName1($data['CUSTOMER NAME 1']);
        $consumer->setName2($data['CUSTOMER NAME 2']);
        $consumer->setUcc($data['UCC']);
        $consumer->setAddress1($data['ADDRESS 1']);
        $consumer->setAddress2($data['ADDRESS 2']);
        $consumer->setDistributor($data['Distributor']);
        $consumer->setPostalDetail($postalDetail);
        $consumer->setCity($city);

        $this->entityManager->persist($consumer);
        $this->entityManager->flush();

        $this->entityManager->commit();

    }

    public function insertCity($name)
    {
        $city = $this->entityManager->getRepository('ParserBundle:City')->findOneBy(array('name' => $name));

        if ($city === null) {
            $city = new City();
            $city->setName($name);

            $this->entityManager->persist($city);
        }

        return $city;
    }

    public function insertPostalDetail($code)
    {
        $postalDetail = $this->entityManager->getRepository('ParserBundle:PostalDetail')->
        findOneBy(array('code' => $code));

        if ($postalDetail === null) {
            $postalDetail = new PostalDetail();
            $postalDetail->setCode($code);

            $this->entityManager->persist($postalDetail);
        }

        return $postalDetail;
    }
}