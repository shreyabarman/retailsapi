<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180814092704 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Package CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_11D55E09DE686795 ON Package (package)');
        $this->addSql('ALTER TABLE RTMOrder CHANGE status status ENUM(\'confirmed\', \'accepted\', \'rejected\', \'delivered\',\'processing\'), CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE Basket CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_25EA554D1BE1FB52 ON Basket (basket_id)');
        $this->addSql('ALTER TABLE User CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ProductSize CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7C2A4966F7C0246A ON ProductSize (size)');
        $this->addSql('ALTER TABLE Brand CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DD93D65C5E237E06 ON Brand (name)');
        $this->addSql('ALTER TABLE Product CHANGE internal_description internal_description VARCHAR(255) DEFAULT NULL, CHANGE app_description app_description VARCHAR(255) DEFAULT NULL, CHANGE SFDC_description SFDC_description VARCHAR(255) DEFAULT NULL, CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1CF73D3177153098 ON Product (code)');
        $this->addSql('ALTER TABLE Consumer CHANGE name_2 name_2 VARCHAR(255) DEFAULT NULL, CHANGE address_2 address_2 VARCHAR(255) DEFAULT NULL, CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_892D557137107B2F ON Consumer (ucc)');
        $this->addSql('ALTER TABLE ProductVariant CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_68D558CE5E237E06 ON ProductVariant (name)');
        $this->addSql('ALTER TABLE ProductType CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7343A255E237E06 ON ProductType (name)');
        $this->addSql('ALTER TABLE City CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D69AD0A5E237E06 ON City (name)');
        $this->addSql('ALTER TABLE BasketItem CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE PostalDetail CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FC8C0F6D77153098 ON PostalDetail (code)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_25EA554D1BE1FB52 ON Basket');
        $this->addSql('ALTER TABLE Basket CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE BasketItem CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('DROP INDEX UNIQ_DD93D65C5E237E06 ON Brand');
        $this->addSql('ALTER TABLE Brand CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('DROP INDEX UNIQ_8D69AD0A5E237E06 ON City');
        $this->addSql('ALTER TABLE City CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('DROP INDEX UNIQ_892D557137107B2F ON Consumer');
        $this->addSql('ALTER TABLE Consumer CHANGE name_2 name_2 VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE address_2 address_2 VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('DROP INDEX UNIQ_11D55E09DE686795 ON Package');
        $this->addSql('ALTER TABLE Package CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('DROP INDEX UNIQ_FC8C0F6D77153098 ON PostalDetail');
        $this->addSql('ALTER TABLE PostalDetail CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('DROP INDEX UNIQ_1CF73D3177153098 ON Product');
        $this->addSql('ALTER TABLE Product CHANGE internal_description internal_description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE app_description app_description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE SFDC_description SFDC_description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('DROP INDEX UNIQ_7C2A4966F7C0246A ON ProductSize');
        $this->addSql('ALTER TABLE ProductSize CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('DROP INDEX UNIQ_7343A255E237E06 ON ProductType');
        $this->addSql('ALTER TABLE ProductType CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('DROP INDEX UNIQ_68D558CE5E237E06 ON ProductVariant');
        $this->addSql('ALTER TABLE ProductVariant CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE RTMOrder CHANGE status status VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE User CHANGE updated_at updated_at DATETIME NOT NULL');
    }
}
