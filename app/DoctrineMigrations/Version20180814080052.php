<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180814080052 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Package CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE RTMOrder CHANGE status status ENUM(\'confirmed\', \'accepted\', \'rejected\', \'delivered\',\'processing\'), CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE Basket ADD updated_at DATETIME NOT NULL, DROP last_updated_at');
        $this->addSql('ALTER TABLE User CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE ProductSize CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE Brand ADD updated_at DATETIME NOT NULL, DROP last_updated_at');
        $this->addSql('ALTER TABLE Product CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE Consumer ADD updated_at DATETIME NOT NULL, DROP last_updated_at, CHANGE name_2 name_2 VARCHAR(255) NOT NULL, CHANGE address_2 address_2 VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE ProductVariant ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, ADD deleted TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE ProductType CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE City CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE BasketItem CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE PostalDetail CHANGE updated_at updated_at DATETIME NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Basket ADD last_updated_at DATETIME NOT NULL, DROP updated_at');
        $this->addSql('ALTER TABLE BasketItem CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE Brand ADD last_updated_at DATETIME NOT NULL, DROP updated_at');
        $this->addSql('ALTER TABLE City CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE Consumer ADD last_updated_at DATETIME NOT NULL, DROP updated_at, CHANGE name_2 name_2 VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE address_2 address_2 VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE Package CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE PostalDetail CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE Product CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE ProductSize CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE ProductType CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE ProductVariant DROP created_at, DROP updated_at, DROP deleted');
        $this->addSql('ALTER TABLE RTMOrder CHANGE status status VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE User CHANGE updated_at updated_at DATETIME NOT NULL');
    }
}
