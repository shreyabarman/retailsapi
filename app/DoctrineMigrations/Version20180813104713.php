<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180813104713 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Package CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE RTMOrder CHANGE status status ENUM(\'confirmed\', \'accepted\', \'rejected\', \'delivered\',\'processing\')');
        $this->addSql('ALTER TABLE Basket CHANGE last_updated_at last_updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE User CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE ProductList CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE ProductSize CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE Brand CHANGE last_updated_at last_updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE Consumer CHANGE name_2 name_2 VARCHAR(255) NOT NULL, CHANGE address_2 address_2 VARCHAR(255) NOT NULL, CHANGE last_updated_at last_updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE ProductType CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE City CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE BasketItem CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE PostalDetail CHANGE updated_at updated_at DATETIME NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Basket CHANGE last_updated_at last_updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE BasketItem CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE Brand CHANGE last_updated_at last_updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE City CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE Consumer CHANGE name_2 name_2 VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE address_2 address_2 VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE last_updated_at last_updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE Package CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE PostalDetail CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE ProductList CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE ProductSize CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE ProductType CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE RTMOrder CHANGE status status VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE User CHANGE updated_at updated_at DATETIME NOT NULL');
    }
}
